import XCTest


protocol Component {

    func operation() -> String
}

class ConcreteComponent: Component {

    func operation() -> String {
        return "ConcreteComponent"
    }
}


class Decorator: Component {

    private var component: Component

    init(_ component: Component) {
        self.component = component
    }

 
    func operation() -> String {
        return component.operation()
    }
}



class ConcreteDecoratorA: Decorator {


    override func operation() -> String {
        return "ConcreteDecoratorA(" + super.operation() + ")"
    }
}


class ConcreteDecoratorB: Decorator {

    override func operation() -> String {
        return "ConcreteDecoratorB(" + super.operation() + ")"
    }
}


class Client {
 
    static func someClientCode(component: Component) {
        print("Result: " + component.operation())
    }
  
}


class DecoratorConceptual: XCTestCase {

    func testDecoratorConceptual() {
 
        print("Client: Tengo un componente simple")
        let simple = ConcreteComponent()
        Client.someClientCode(component: simple)

       
     
        let decorator1 = ConcreteDecoratorA(simple)
        let decorator2 = ConcreteDecoratorB(decorator1)
        print("\nClient: Tengo un Componente Decorado")
        Client.someClientCode(component: decorator2)
    }
}